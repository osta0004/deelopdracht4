<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Registreren</title>
		<script src="jquery-1.11.3.min.js"></script>
		<script src="jquery.validate.js"></script>
	</head>
	<body>
		<form action="" name="input" id="registerform" method="post">
			<h1>Registreren</h1>
			<input type="hidden" name="send" value="true"/>
			<table>
			<tr>
				<td><label for="username">Username:</label></td>
				<td><input type="text" id="username" name="username" minlength="3" required/></td>
				<td><span id="usernametext"></div></td>
				
			</tr>
			<br>
			<br>
			<tr>
				<td><label for="password">Password:</label></td>
				<td><input type="password" id="password" name="password" minlength="3" required/></td>
				<td><span id="passwordValResult"></span></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" value="Versturen" /></td>
			</tr>
			</table>
		</form>
		<script>
			$("#registerform").validate();
			
		</script>
	</body>
</html>