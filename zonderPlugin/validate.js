$(document).ready(function() {

	
	$("#username").keyup(function validateUsernamecheck() {
		if ($("#username").val().length < 3) {
			$("#usernamemessage").text("Je moet minstens 3 tekens invoeren");
			$("#usernamemessage").css('color', '#BA2A2A');
			$("#username").css('border-color', '#BA2A2A');
			$("#username").css('box-shadow', '5px 5px 15px #FF5555');
			
		
		}else if ($("#resultsql").val() == 'true') {
			$("#usernamemessage").text("Username bestaat al");
			$("#usernamemessage").css('color', '#BA2A2A');
			$("#username").css('border-color', '#BA2A2A');
			$("#username").css('box-shadow', '5px 5px 15px #FF5555');
		
		} else {
			$("#usernamemessage").text("Gevalideerd");
			$("#usernamemessage").css('color', '#2EB82E');
			$("#username").css('border-color', '#2EB82E');
			$("#username").css('box-shadow', '5px 5px 15px #66FF33');

		}
	});

	$("#password").keyup(function validatePassword() {
		if ($("#password").val().length < 3) {
			$("#passwordmessage").text("Je moet minstens 3 tekens invoeren");
			$("#passwordmessage").css('color', '#BA2A2A');
			$("#password").css('border-color', '#BA2A2A');
			$("#password").css('box-shadow', '5px 5px 15px #FF5555');

		} else {
			$("#passwordmessage").text("Gevalideerd");
			$("#passwordmessage").css('color', '#2EB82E');
			$("#password").css('border-color', '#2EB82E');
			$("#password").css('box-shadow', '5px 5px 15px #66FF33');

		}
	});

	$("#confirmpassword").keyup(function validateconfirmPassword() {
		if ($("#password").val().length == 0) {
			$("#confirmpasswordmessage").text("Voer eerst een wachtwoord in");
			$("#confirmpasswordmessage").css('color', '#BA2A2A');
			$("#confirmpassword").css('border-color', '#BA2A2A');
			$("#confirmpassword").css('box-shadow', '5px 5px 15px #FF5555');

		} else if ($("#password").val().length < 3) {
			$("#confirmpasswordmessage").text("Voer eerst een gevalideerd wachtwoord in");
			$("#confirmpasswordmessage").css('color', '#BA2A2A');
			$("#confirmpassword").css('border-color', '#BA2A2A');
			$("#confirmpassword").css('box-shadow', '5px 5px 15px #FF5555');

		} else if ($("#confirmpassword").val() != $("#password").val()) {
			$("#confirmpasswordmessage").text("Wachtwoorden zijn niet hetzelfde");
			$("#confirmpasswordmessage").css('color', '#BA2A2A');
			$("#confirmpassword").css('border-color', '#BA2A2A');
			$("#confirmpassword").css('box-shadow', '5px 5px 15px #FF5555');

		} else {
			$("#confirmpasswordmessage").text("Gevalideerd");
			$("#confirmpasswordmessage").css('color', '#2EB82E');
			$("#confirmpassword").css('border-color', '#2EB82E');
			$("#confirmpassword").css('box-shadow', '5px 5px 15px #66FF33');

		}
	});

	// $("#email").blur(function(){
	// if($("#email").val()!= [a-z]+"@"+[a-z]+"."+[a-z]){
	// $("#emailmessage").text("Email niet correct");
	// }else{
	// $("#emailmessage").text("Email correct");
	// }
	// });

	$("#firstname").keyup(function validateFirstname() {
		if ($("#firstname").val().length == 0) {
			$("#firstnamemessage").text("Verplicht veld");
			$("#firstnamemessage").css('color', '#BA2A2A');
			$("#firstname").css('border-color', '#BA2A2A');
			$("#firstname").css('box-shadow', '5px 5px 15px #FF5555');
			return false;
		} else {
			$("#firstnamemessage").text("Gevalideerd");
			$("#firstnamemessage").css('color', '#2EB82E');
			$("#firstname").css('border-color', '#2EB82E');
			$("#firstname").css('box-shadow', '5px 5px 15px #66FF33');

		}
	});

	$("#lastname").keyup(function validateLastname() {
		if ($("#lastname").val().length == 0) {
			$("#lastnamemessage").text("Verplicht veld");
			$("#lastnamemessage").css('color', '#BA2A2A');
			$("#lastname").css('border-color', '#BA2A2A');
			$("#lastname").css('box-shadow', '5px 5px 15px #FF5555');
			lastname = false;
		} else {
			$("#lastnamemessage").text("Gevalideerd");
			$("#lastnamemessage").css('color', '#2EB82E');
			$("#lastname").css('border-color', '#2EB82E');
			$("#lastname").css('box-shadow', '5px 5px 15px #66FF33');

		}
	});
	
	

		//php script aanroepen met AJAX

		$("#button").click(function() {
			$.ajax({
				type : 'POST',
				url : "insertdata.php",
				async : true,
				datatype : 'text',

				data : {
					username : $('#username').val(),
					password : $('#password').val(),
					confirmpassword : $('#confirmpassword').val(),
					firstname : $('#firstname').val(),
					lastname : $('#lastname').val(),
				},
				succes : function(data) {
					$("#registermessage").text("Registratie succesvol");
					$("#registermessage").css('color', '#2EB82E');
					
					
				},
				error : function(XMLHttpRequest, textStatus, errorThrown) {
					$("#registermessage").text("Registratie niet succesvol");
					$("#registermessage").css('color', '#BA2A2A');
				}
			});

		});
		$('#registermessage').load('check_username.php');
		
		$("#username").keyup(function() {
			$.ajax({
				type : 'POST',
				url : "check_username.php",
				async : false,
				datatype : 'text',

				data : {
					username : $('#username').val(),
					sqlusername: $('#sqlusername').val(),
					resultsql: $('#resultsql').val(),
				},
				succes : function(data) {
					if ($('#resultsql').val() == "false"){
						alert("Succes");
					
					}
					
				},
				error : function(XMLHttpRequest, textStatus, errorThrown) {
						alert("Niet succesvol");
				
				}
			});

		});

	});
$(document).ready(function() {

	$('#registermessage').load('check_username.php').show();
	$("#username").keyup(function () {
		$.post('check_username.php',{username: input.username.value},
		function (result){
			console.log(result);
				$('#registermessage').html(result).show();
		});
	});
	
});


