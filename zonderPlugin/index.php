<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Registreren</title>
		<link type="text/css" rel="stylesheet" href="style.css"/>	
		<script src="jquery-1.11.3.min.js"></script>
		<script src="validate.js"></script>
		<script src="ajax.js"></script>
		
	</head>
	<body>
		<div class="formcss">
		<form action="" name="input" id="registerform" method="post">
			<h1>Registreren</h1>
			
			<input type="hidden" name="send" value="true"/>
			
			<table>
				
			<tr>
				<td><label for="username">Gebruikersnaam:</label></td>
				<td><input type="text" id="username" name="username" autofocus/>
				<div class="username" id="usernamemessage"></div></td>
                <td><div class="registermessage" id="registermessage"></div></td>
			</tr>
		
			</tr>
			<tr>
				<td><label for="firstname">Voornaam:</label></td>
				<td><input type="text" id="firstname" name="firstname" />
				<div class="firstname" id="firstnamemessage"></div></td>
				
			</tr>
			<tr>
				<td><label for="lastname">Achternaam:</label></td>
				<td><input type="text" id="lastname" name="lastname" />
				<div class="lastname" id="lastnamemessage"></div></td>
				
			</tr>
			
			<tr>
				<td><label for="password">Wachtwoord:</label></td>
				<td><input type="password" id="password" name="password" />
				<div class="password" id="passwordmessage"></div></td>
			</tr>
			<tr>
				<td><label for="passwordconfirm">Bevestig Wachtwoord:</label></td>
				<td><input type="password" id="confirmpassword" name="confirmpassword" />
				<div class="confirmpassword" id="confirmpasswordmessage"></div></td>
			</tr>
			<!-- tr>
				<td><label for="email">Email:</label></td>
				<td><input type="text" id="email" name="email"/>
					<div class="emailmessage" id="emailmessage"></div>
				</td>
			</tr> -->
			
			
			
			<tr>
			<div class="button"><td colspan="2"><input type="button" id="button" value="Versturen" /></td></div>	
			</tr>
			
			</table>
	
		</form>
		</div>
		<!-- <tr>
				<div id="resultsql"><?php echo "false" ?> </div>
		</tr> -->
		
		
	</body>
</html>